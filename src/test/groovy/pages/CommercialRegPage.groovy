package pages

import geb.Page


class CommercialRegPage extends Page {
	static at = {waitFor{$("#selectedFacility").find("option",1).displayed}}
		//title == "Precor - Premium Home and Commercial Fitness Equipment"}}

	static content = {
		
		selectFacilityNme {$("#selectedFacility")}
		selectContactNme {$("#contactInfo")}
		selectCustomerSource {$("#customerSource")}
		selectManagementCompany {$("#managementCompany")}
		selectBuyingGroup {$("#buyingGroup")}
		selectDealerRep {$("#dealerRep")}
		txtCustomerType {$("#customerType")}
				
		addProdBtn {$("form",name:"productRegistrationForm").find("fieldset",3).find(".btn.btn-primary")}
		upldBtn {$("#uploadBtn")}
		
		txtSerialNumSearch {$(".modal-dialog").find("#serialNumberId")}
		tblAssets {$(".modal-dialog").find("table",0).find("tbody",0).find("tr",1)} 
		addComponentBtn {$(".modal-dialog").find(".modal-footer").find(".btn.btn-primary.pull-left")}
		saveAndCloseBtn {$(".modal-dialog").find(".modal-footer").find(".btn.btn-warning.pull-right",0)}
		addProdNotListedBtn {$(".modal-dialog").find(".modal-footer").find(".btn.btn-primary.pull-left",1)}
		saveAndAddAnotherBtn {$(".modal-dialog").find(".modal-footer").find(".btn.btn-warning.pull-right",1)}
		closeBtn {$(".modal-dialog").find(".modal-header").find(".close")}
		
		nextBtn {$("form",name:"productRegistrationForm").find(".form-group").find(".btn.btn-primary.ng-scope")}
		
		
		lstNewProdType {$("form",name:"AddNewProductnotListedForm").find("#productType")}
		txtProdSrlNum {$("form",name:"AddNewProductnotListedForm").find("#productSerialNumber")}
		txtProdDescr {$("form",name:"AddNewProductnotListedForm").find("#productDescription")}
		addBtn {$("form",name:"AddNewProductnotListedForm").find(".modal-footer").find(".btn.btn-primary")}
		okBtn {$(".modal-content").find(".btn.btn-primary")}
	}

	void fnEnterFacilityInfo(String facilityName,String contactEmailDet) 
	{	
		waitFor{selectFacilityNme.displayed}
			
		//select facility name
		selectFacilityNme.click()
		waitFor{selectFacilityNme.find("option",text:contains(facilityName)).displayed}
		
		selectFacilityNme.find("option",text:contains(facilityName)).click()
		
		//Wait for customer type
		waitFor{txtCustomerType.value().length()> 0}
		println "Customer Type: " + txtCustomerType.value()
		
		//select contact name
		selectContactNme.click()
		waitFor{selectContactNme.find("option",text:contains(contactEmailDet)).displayed}
		selectContactNme.find("option",text: contains(contactEmailDet)).click()
		
		Thread.sleep(500)
				
	}
	
	Void fnEnterSaleInfo(String saleDetls)
	{
		def arrSaleDet=saleDetls.split(";")
		
		
		
		//Enter sell date, Install date and Invoice number
		$("input", name: "sellDate").value(arrSaleDet[0])
		$("input", name: "installDate").value(arrSaleDet[1])
		$("input", name: "invoiceNumber").value(arrSaleDet[2])
				
		//Select sale info - Customer source, MG, BG and Rep
		//selectCustomerSource.find("option",2).click()
			
		//Select Customer source	
		if(arrSaleDet[3]!=" ")
		{					
			waitFor{selectCustomerSource.find("option",text:contains(arrSaleDet[3])).displayed}
			selectCustomerSource.find("option",text:contains(arrSaleDet[3])).click()
		}
					
		
		//Select Management Company
		if(arrSaleDet[4]!=" ")
		{
			waitFor{selectManagementCompany.find("option",text:contains(arrSaleDet[4])).displayed}
			selectManagementCompany.find("option",text:contains(arrSaleDet[4])).click()
		}
		
		//Select Buying Group
		if(arrSaleDet[5]!=" ")
		{
			waitFor{selectBuyingGroup.find("option",text:contains(arrSaleDet[5])).displayed}
			selectBuyingGroup.find("option",text:contains(arrSaleDet[5])).click()
		}
		
		//Select Dealer Rep
		if(arrSaleDet[6]!=" ")
		{
			waitFor{selectDealerRep.find("option",text:contains(arrSaleDet[6])).displayed}
			selectDealerRep.find("option",text:contains(arrSaleDet[6])).click()
		}
		
		Thread.sleep(4000)
	}
	Void fnAddProduct(String assetsLst)
	{
		addProdBtn.click()
		waitFor{txtSerialNumSearch.displayed}
		
		//components list
		String[] compntLst = assetsLst.split("&")
		
		fnSearchNAddComponent(compntLst)
						
		//Click on Next button
		nextBtn.click()
		Thread.sleep(2000)
		
	}
	
	Void fnSearchNAddComponent(String[] compntLst)
	{
		def var
		def assetsLength = compntLst.size()
		println "Assets size: " + assetsLength
	
		for(var=0;var<assetsLength;var++)
		{
			if(compntLst[var].contains(";"))
			{
				String[] compositeCompVal=compntLst[var].split(";")
				txtSerialNumSearch.value(compositeCompVal[0])
				waitFor{tblAssets.displayed}
			
				//click on first row and click Add component
				tblAssets.click()
				addComponentBtn.click()
				Thread.sleep(500)
				
				txtSerialNumSearch.value(compositeCompVal[1])
				waitFor{tblAssets.displayed}
			
				//click on first row and click Add component	
				tblAssets.click()
				Thread.sleep(500)
				addComponentBtn.click()
				
			}
			else
			{
				txtSerialNumSearch.value(compntLst[var])
				waitFor{tblAssets.displayed}
			
				//click on first row and click Add component
				tblAssets.click()
				Thread.sleep(500)
				addComponentBtn.click()
			}
			
			//Click Save and close button
			saveAndAddAnotherBtn.click()
			Thread.sleep(500)
			
		}
		
		
		//Click Save and close button
		closeBtn.click()
		Thread.sleep(1000)
	//	waitFor{nextBtn.displayed}
		
	}
	
	Void fnUploadInvoice()
	{
		//upldBtn.click()
		/*withWindow({title.toString()=="File Upload"})
		{
			println "Upload button clicked"
			$("#inputFile").text="C:\\Users\\mazeema\\Downloads\\5b974ae5-04c3-4a5b-b4ab-7974fe565e6e"
		}*/
		
		
	}
	
	/*Void fnAddNewFacility()
	{
		
	}*/
	
	Void fnVerifyAddProdNotListedPopup()
	{
		waitFor{addProdBtn.displayed}
		addProdBtn.click()
		waitFor{txtSerialNumSearch.displayed}
		
		addProdNotListedBtn.click()		
		waitFor{lstNewProdType.displayed}
		Thread.sleep(500)
				
	}
	
	Void fnAddProdNotListed(String newProdDet)
	{
		def arrNewProdDet=newProdDet.split(";")
		
		def prodTypeVal=arrNewProdDet[0]
		def srlNumber=arrNewProdDet[1]
		def prodDescr=arrNewProdDet[2]
					
		
//		lstNewProdType.click()	
		waitFor{lstNewProdType.find("option",text:contains(prodTypeVal)).displayed}
		lstNewProdType.find("option",text:contains(prodTypeVal)).click()				
		Thread.sleep(500)	
			
		txtProdSrlNum.value(srlNumber)
		txtProdDescr.value(prodDescr)
		
		addBtn.click()
		//Thread.sleep(500)
				
		waitFor{okBtn.displayed}
		okBtn.click()
					
		waitFor{tblAssets.displayed}		
	//	Thread.sleep(1500)
	}
	
}

