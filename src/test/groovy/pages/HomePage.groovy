package pages

import geb.Page

class HomePage extends Page {
	static at = {waitFor{title == "Precor"}}

	static content = {
		partnerTools {$("#home_loggedin").find("a",4)}
		
	}

	void selectPartnerTools() {
		partnerTools.click()
		println driver.getTitle();
	//	switchToNewWindow("Precor USA Staging - Sign In")
		String winHandleBefore = driver.getWindowHandle();
		
				for(String winHandle : driver.getWindowHandles()){
					driver.switchTo().window(winHandle)
					if(driver.getTitle()=="Precor USA Staging - Sign In")
					break
				}
				println driver.getTitle();
				
	}
	
	void onUnload(PrecorPremiumHomePage nextPage) {
		newPageName = nextPage.class.simpleName
	};
}
